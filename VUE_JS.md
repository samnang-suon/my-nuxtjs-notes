# VueJS Cheat Sheet
## Data Binding
### One-Way Binding
#### Code
![VueJSOneWayBinding](images/VueJSOneWayBinding.png)
#### Preview
![VueJSOneWayBindingPreview](images/VueJSOneWayBindingPreview.png)

### Two-Way Binding
#### Code
![VueJSTwoWayBinding](images/VueJSTwoWayBinding.png)
#### Preview
![VueJSTwoWayBindingPreview](images/VueJSTwoWayBindingPreview.png)

## Event Binding
### Code
![VueJSEventBinding](images/VueJSEventBinding.png)
### Preview
![VueJSEventBindingPreview](images/VueJSEventBindingPreview.png)

For more info: https://michaelnthiessen.com/redirect-in-vue/

## CSS Style Binding
### Code
![VueJSStyleBinding](images/VueJSStyleBinding.png)
### Preview
![VueJSStyleBindingPreview01](images/VueJSStyleBindingPreview01.png)

![VueJSStyleBindingPreview02](images/VueJSStyleBindingPreview02.png)

## Key Binding
### Code
![VueJSKeyBinding](images/VueJSKeyBinding.png)
### Preview
![VueJSKeyBindingPreview](images/VueJSKeyBindingPreview.png)

## V-for
### Code
![VueJSLoop](images/VueJSLoop.png)
### Preview
![VueJSLoopPreview](images/VueJSLoopPreview.png)

## V-if
### Code
![VueJSConditional](images/VueJSConditional.png)
### Preview
![VueJSConditionalPreview](images/VueJSConditionalPreview.png)

For showing image using v-bind:
* https://stackoverflow.com/questions/55152795/vue-js-data-binding-not-working-for-img-src
* https://nuxtjs.org/docs/2.x/directory-structure/assets/#images

## Data Model
![VueJSDataModel](images/VueJSDataModel.png)

## Computed Function
### Code
![VueJSComputedFunction](images/VueJSComputedFunction.png)
### Preview
![VueJSComputedFunctionPreview](images/VueJSComputedFunctionPreview.png)

## Component Lifecycle
![VueJSComponentLifeCycle](images/VueJSComponentLifeCycle.png)

## Component Communication: Parent to Child
### Child Code
![VueJSComponentCommunicationParentToChild01](images/VueJSComponentCommunicationParentToChild01.png)
### Parent Code
![VueJSComponentCommunicationParentToChild02](images/VueJSComponentCommunicationParentToChild02.png)
### Preview
![VueJSComponentCommunicationParentToChildPreview](images/VueJSComponentCommunicationParentToChildPreview.png)

For more info: https://medium.com/js-dojo/component-communication-in-vue-js-ca8b591d7efa

## Component Communication: Child to Parent
### Child Code
![VueJSComponentCommunicationChildToParent01](images/VueJSComponentCommunicationChildToParent01.png)
### Parent Code
![VueJSComponentCommunicationChildToParent02](images/VueJSComponentCommunicationChildToParent02.png)
### Preview
![VueJSComponentCommunicationChildToParentPreview](images/VueJSComponentCommunicationChildToParentPreview.png)

For more info: https://medium.com/js-dojo/component-communication-in-vue-js-ca8b591d7efa

## Component Communication: Sibling to Sibling
### Sibling 01 Code
![VueJSComponentCommunicationSiblingToSibling01](images/VueJSComponentCommunicationSiblingToSibling01.png)
### Sibling 02 Code
![VueJSComponentCommunicationSiblingToSibling02](images/VueJSComponentCommunicationSiblingToSibling02.png)
### Parent Code
![VueJSComponentCommunicationSiblingToSibling03](images/VueJSComponentCommunicationSiblingToSibling03.png)
### Preview
![VueJSComponentCommunicationSiblingToSiblingPreview](images/VueJSComponentCommunicationSiblingToSiblingPreview.png)

For more info: https://medium.com/js-dojo/component-communication-in-vue-js-ca8b591d7efa

## Vuex Overview
![VuexIntro](images/VuexIntro.png)
### Folder Structure for Modules
![VuexStoreFolder](images/VuexStoreFolder.png)
### State.js
![VuexStateJS](images/VuexStateJS.png)
### Getters.js
![VuexGettersJS](images/VuexGettersJS.png)
### Mutations.js
![VuexMutationsJS](images/VuexMutationsJS.png)
### Actions.js
![VuexActionsJS](images/VuexActionsJS.png)
### Import
![VuexIndex](images/VuexIndex.png)
### References
* https://nuxtjs.org/docs/2.x/directory-structure/store/
* https://vuex.vuejs.org/guide/modules.html

## Axios
![AxiosDemo](images/AxiosDemo.png)

For more info:
* https://www.npmjs.com/package/axios
* https://axios.nuxtjs.org/usage
* https://github.com/public-apis/public-apis#animals

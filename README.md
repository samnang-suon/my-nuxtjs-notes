# My NuxtJS Notes
## Pre-requisites
* TCP/IP
* HTTP
* URL vs URI vs URN: 
   * https://danielmiessler.com/study/difference-between-uri-url/
   * https://yeonghoey.com/http/url/
* VUE_JS ---> **NOTE** NuxtJS builds on top of VueJS, so you DO need to have some VueJS Foundations. See: [My VUE_JS README](VUE_JS.md)
## Installation
1. You need NodeJS + NPM ---> https://nodejs.org/en/
2. Open CMD or Git Bash and check your NPM version using:

        npm --version

3. To make sure that you are using the latest version, use:

        npm install --global npm

source: https://stackoverflow.com/questions/6237295/how-can-i-update-nodejs-and-npm-to-the-next-versions/6237400#6237400

4. Install Create-Nuxt-App using

        npm install --global create-nuxt-app

source: https://github.com/nuxt/create-nuxt-app

Check the tool version:

      create-nuxt-app --version

![CreateNuxtAppVersion](images/CreateNuxtAppVersion.png)

5. List your globally installed NPM packages using:

        npm list --global --depth 0

![ListGlobalInstalledNpmPackages](images/ListGlobalInstalledNpmPackages.png)

source: https://medium.com/@alberto.schiabel/npm-tricks-part-1-get-list-of-globally-installed-packages-39a240347ef0
## Create a new project
1. Use the following command:

         create-nuxt-app [YouProjectName]
## NuxtJS Folder Structure
![NuxtFolderStructure](images/NuxtFolderStructure.png)

| Folder Name | Description |
|-------------|-------------|
assets | The assets directory contains your uncompiled assets such as your styles, images, or fonts.
components | The components directory is where you put all your Vue.js components which are then imported into your pages.
layouts | Layouts are a great help when you want to change the look and feel of your Nuxt.js app. Whether you want to include a sidebar or have distinct layouts for mobile and desktop.
middleware | The middleware directory contains your application middleware. Middleware lets you define custom functions that can be run before rendering either a page or a group of pages (layout).
pages | The pages directory contains your application's views and routes. As you've learned in the last chapter, Nuxt.js reads all the .vue files inside this directory and uses them to create the application router.
plugins | The plugins directory contains JavaScript plugins that you want to run before instantiating the root Vue.js Application. This is the place to add Vue plugins and to inject functions or constants. Every time you need to use Vue.use(), you should create a file in plugins/ and add its path to plugins in nuxt.config.js.
static | The static directory is directly mapped to the server root and contains files that have to keep their names (e.g. robots.txt) or likely won't change (e.g. the favicon)
store | The store directory contains your Vuex Store files. The Vuex Store comes with Nuxt.js out of the box but is disabled by default. Creating an  index.js  file in this directory enables the store.

References:
* https://nuxtjs.org/docs/2.x/get-started/directory-structure/
* https://nuxtjs.org/docs/2.x/directory-structure/layouts
* https://nuxtjs.org/docs/2.x/directory-structure/middleware
* https://nuxtjs.org/docs/2.x/directory-structure/plugins
* https://nuxtjs.org/docs/2.x/directory-structure/store

## Dynamic Routing
### Folder Structure
![NuxtJSDynamicRoutingFolderStructure](images/NuxtJSDynamicRoutingFolderStructure.png)
### Code
![NuxtJSDynamicRouting](images/NuxtJSDynamicRouting.png)
### Dynamic Routing Validation
To make sure that your users are only using numbers, use the following:

![NuxtJSDynamicRoutingValidation](images/NuxtJSDynamicRoutingValidation.png)

For more info: https://nuxtjs.org/docs/2.x/components-glossary/pages-validate/

## Navigation Links
### Static
![NuxtJSNavigationLinks](images/NuxtJSNavigationLinks.png)
### Dynamic/Programmatically
![NuxtJSDynamicNavigationLink](images/NuxtJSDynamicNavigationLink.png)

## Nested Pages
### Folder Structure
![NuxtJSNestedPages01](images/NuxtJSNestedPages01.png)
### Code
![NuxtJSNestedPages04](images/NuxtJSNestedPages04.png)
### View
![NuxtJSNestedPages02](images/NuxtJSNestedPages02.png)

![NuxtJSNestedPages03](images/NuxtJSNestedPages03.png)

For more info: https://nuxtjs.org/examples/routing-nested-pages/

## The Layout Property
### Folder Structure
![NuxtJSLayoutFolder](images/NuxtJSLayoutFolder.png)
### Code
![NuxtJSLayoutProperty](images/NuxtJSLayoutProperty.png)

For more info: https://nuxtjs.org/docs/2.x/components-glossary/pages-layout/

## The Error Page
![NuxtJSDefaultErrorPage](images/NuxtJSDefaultErrorPage.png)

## Import Component
![NuxtJSImportComponent](images/NuxtJSImportComponent.png)

For more info: https://nuxtjs.org/docs/2.x/directory-structure/components/

## Why Use Scoped Style?
For more info: https://vue-loader.vuejs.org/guide/scoped-css.html

## Global CSS Style
![NuxtJSGlobalCSSStyle01](images/NuxtJSGlobalCSSStyle01.png)

![NuxtJSGlobalCSSStyle02](images/NuxtJSGlobalCSSStyle02.png)

## Assets Folder Usages
### Images
![NuxtJSAssetsExample01](images/NuxtJSAssetsExample01.png)

## Navigation Link Highlight
### Code
![NuxtJSNavigationLinkHighlight](images/NuxtJSNavigationLinkHighlight.png)
### Preview
![NuxtJSNavigationLinkHighlightPreview](images/NuxtJSNavigationLinkHighlightPreview.png)

## EXTRAS
* How to add Bootstrap to NuxtJS ---> https://bootstrap-vue.org/docs#nuxtjs-module
* How to add Bootstrap Icons to NuxtJS ---> https://bootstrap-vue.org/docs#icons
* How to add Font-Awesome to NuxtJS ---> https://www.npmjs.com/package/@nuxtjs/fontawesome
* How to add JSON Web Token (JWT) to NuxtJS ---> https://auth.nuxtjs.org/providers/laravel-jwt/
